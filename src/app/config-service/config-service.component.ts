
import { Config } from '../dto/config.dto';
import {Observable} from 'rxjs/Observable';
import { ConfigService} from './../config.service';
import { Component} from '@angular/core';


@Component({
  selector: 'app-config-service',
  templateUrl: './config-service.component.html',
  styleUrls: ['./config-service.component.css']
})
export class ConfigServiceComponent {

  constructor(private configService: ConfigService, private configDto: Config) {

   }

   // observables are synonymous with functions
   // lazily initialized and can be used to return mutiple values over time
   // onDestroy(){subsciber.unsubscirbe()} -> disposes the observable on component destroy thereby preventing memory leaks
   // below all in one line

   // var observable = Rx.Observable.create(function subscribe(observer) {
  // observer.next(1);
  // observer.next(2);
   // observer.next(3);
  // observer.next(3);
  // observer.complete();
  // observer.next(4); // Is not delivered because it would violate the contract


  getConfigValues(event: Event) {
    console.log(event);
    const sam: Observable<JSON> = this.configService.getConfigFile()
    .subscribe(data => console.log('samole', data) );
    //for each is not a function
    //sam.forEach(i => console.log(i));
    console.log( this.configDto);
  }


}
