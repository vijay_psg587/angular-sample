import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class ConfigService {
  configUrl = './../assets/config.json';
  constructor(private http: HttpClient) {

  }

  getConfigFile(): any {
    console.log("Inside config get");
    return this.http.get(this.configUrl);

  }
}
