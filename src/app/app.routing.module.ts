import { RouterModule, Routes, Router } from '@angular/router';
import { SearchPromoComponent } from './search-promo/search-promo.component';
import { FormAppComponent } from './form-app/form-app.component';
import { NgModule } from '@angular/core';

export const routes: Routes = [

  {path : '' , redirectTo: 'form', pathMatch: 'full'},
  {path : 'other' , component: FormAppComponent},
  {path : '', component : FormAppComponent},
  {path : 'search', component: SearchPromoComponent},
  {path : 'form', component: FormAppComponent},
  // adding a parameterized link param
  {path : 'search/:term', component: SearchPromoComponent},
  // incase of routing not working after deployment. i.e 404 error page being thrown
  // the real issue is that the server tries to handle the requests first and since it can and will
  // never find the path provided and whatever the path maybe we shall never see the page
  // and this causes a 404 error page
  // by default all our paths (rotuing) should be handled by angular instead of browser
  // hence catch the unknown paths in server and try to route it to angular like this
  { path : '**', redirectTo: 'form'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports : [RouterModule]

})

export class AppRoutingModule {

}
