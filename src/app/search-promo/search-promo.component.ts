import { NgxLoggerLevel } from 'ngx-logger';
import { retry } from 'rxjs/operator/retry';
import { Subject } from 'rxjs/Subject';
import { ActivatedRoute } from '@angular/router';
import { setTimeout } from 'timers';


import { Response } from '@angular/http';
import { _switch } from 'rxjs/operator/switch';
import { any } from 'codelyzer/util/function';
import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { debounceTime, filter, flatMap, map, switchMap, tap } from 'rxjs/operators';

import { Promotion } from './../dto/promo.dto';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SearchService } from './../search.service';
import { AfterViewChecked, AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-search-promo',
  templateUrl: './search-promo.component.html',
  styleUrls: ['./search-promo.component.css']
})
export class SearchPromoComponent implements  AfterViewInit, OnDestroy {
  searchGroup: FormGroup;
  stringify  =  require('json-stringify-safe');
  someResult: any;
  promoList: Promotion[];
  subsc: Subscription;
  resp: Response;
  localInput = new FormControl('', Validators.required);
  consumedPromoList: Observable<Promotion[]>;
  loading: Boolean = false;

  //subjects are both observers and observables
  //so here we can define a subject that needs to subscribe to an observable
  // basically as a subject or behavior subject
  compSubject: Subject<any>;
  // Use activated route to get the data from
  constructor(private searchService: SearchService, fb: FormBuilder, private paramConfig: ActivatedRoute,
    private logger: NGXLogger
  ) {
    this.searchGroup = fb.group({
      'localInput': this.localInput
    });
     this.stringify = require('json-stringify-safe');

     // FOLLOW THE ABOVE SAMPLE FOR BEHAVIOR SUBJECT
     // WHICH EMITS THE LATEST VALUE
      this.searchService.bSubject.
      subscribe(resp => {
        console.log('got value in comp', resp);
        this.logger.debug('this is the first debug log step', resp);
        return resp;
      }
      );

      this.searchService.subject.subscribe(resp =>  {
        console.log('actual value of respose', resp);
        // now set a sample boolean values to make sure that we got the response from Http call
        this.loading = true;
        if(resp != null && resp.length >0 && resp instanceof Array){
          this.promoList = resp;
        }

      });

      if(this.promoList != null && this.promoList.length > 0){
        console.log('now the value of promoList', this.promoList);
      } else {
        this.loading = false;
      }


    //  this.searchService.getPromoListAsNext().
    //   subscribe(data => {
    //    // return data.forEach(i => console.log('fettch', i));
    //    return data;
    //   }
    //   );

      console.log('promolist valie', this.subsc);
    //   this.searchService.getPromoList().subscribe(
    //    resp => {
    //      this.someResult = <Promotion[]>resp;
    //      console.log("Inside sub in comp"+this.stringify(resp));
    //      console.log("before calling the methid");
    //      this.callDto(<Promotion[]>resp);
    //    }
    //  );
     console.log('inside const()' + this.stringify(this.someResult));
   }



    ngAfterViewInit(){

    }


    ngAfterViewContentChecked() {
    console.log('inside AfterViewContent()' + this.stringify(this.someResult));
  }
    ngOnDestroy(){
      // unsubscription should be done here else will result in a memory leak
      // declare a variable first and assign the serch
      //this.searchService.subject.unsubscribe();
    }

}
