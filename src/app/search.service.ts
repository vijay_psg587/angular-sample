import { error } from 'util';
import { Subject } from 'rxjs/Subject';

import { Response } from '@angular/http';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {Promotion} from './dto/promo.dto';
import { List } from 'immutable';
import {map, filter, reduce, switchMap, tap} from 'rxjs/operators';

/*
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/pipe';

these are dot operator imports which gets patched to Observable.prototype
say when a operator is not used but import statement remains, then the operators patched to Observable.pattern
remains (removing unused is called DOM tree shaking)

To overcome this from Rxjs 5.5 pipepable operators are introduced
. they can be imported as above
they are pure functions and hence Observable.prototype will not be patched
 * npm install immutable to install immutable moduel
 * if no ts file found , then rename the .js to ts and then import it as above
 */

@Injectable()
export class SearchService {
  getUrl: String = './../assets/promotionList.json';
  subject: BehaviorSubject<Promotion[]> ; // initialize with an empty response[]
  subjectAsObservable;
  stringify  = require('json-stringify-safe');
   bSubject = new BehaviorSubject("a");
   someResult;
   promoList:Promotion[];
   promotion:Promotion;
  constructor(private http: HttpClient) {



    //we first initialize the behavior subject with def empty values
    //if no value is emitted then def value is used whihc is empty

    this.promotion = new Promotion(5,'vv','des');
    this.promoList = new Array<Promotion>();
    this.promoList.push(this.promotion);


    this.subject = new BehaviorSubject<Promotion[]>(this.getPromoList());
    // first subscription emits the Promotion default that is defined above
    // tried with empty and works like a charm
    this.getPromoListAsNext();
    this.method1();
    this.subject.subscribe(data => console.log('data in first constr', data));

    //passing the function that returns an observable as a param
    this.anotherMethod1();
    this.anotherMethod2();
    this.anotherMethod3();

  }

    // getPromolist returns an Observable and hence passed in the next of subject
    getPromoListAsNext() {
       this.getPromoList().subscribe(data => {
         this.someResult = data;
         console.log('someresult', this.someResult);
         // only after a sec these values are populated
         // so calling the metod1() here instead of constr makes sense
         this.method1();
      });

    }

    method1(){
      console.log("getting value of someresult in another method", this.someResult);
      this.subject.next(this.someResult);
    }

    anotherMethod2(){
       this.bSubject.next("c"); // Subscription got c
       this.bSubject.next("d"); // Subscription got d
    }

    anotherMethod1(){
      this.bSubject.next("b");
    }

    anotherMethod3(){
      this.bSubject.subscribe((value) => {
      console.log("Subscription got", value); // Subscription got b,
                                          // ^ This would not happen
                                          // for a generic observable
                                          // or generic subject by default
      });
    }

    anotherMethod4(){
      this.bSubject.subscribe((value) => {
      console.log("Subscription got 4", value); // Subscription got b,
                                          // ^ This would not happen
                                          // for a generic observable
                                          // or generic subject by default
      });
    }

    getPromoList(){
    // by default it emits a response of Json and hence Observabpe<PromotionList>
    // throws an error,
    // I am using switchMap to switch to another Observable  i.e the http.get on subscirption

    // this.http.get(`${this.getUrl}`).map(data => console.log('inside first', data));
    // If I use map like above then I consume the stream at that time and there will no data to output while subscribing
      this.someResult = this.http.get(`${this.getUrl}`);
       console.log("inside promoList", this.someResult);
       return this.someResult;
    // console.log("first subsriber"+JSON.stringify (this.someResult);

  }


  // getPromoValues(): Promotion[]{
  //   //Now the observable emits a responsejson
  //   //So always use it after the emitting it

  //   // now http.get returns an observable
  //   // so use of Observable.from(this.http.get(SOME_ULR)) will give another observable
  //   // basically an observable<observable<Promotion[]>>>
  //   // so just using as it is


  //   return this.getPromoList()
  //     .pipe(tap(data => console.log("inside tap"+ JSON.stringify(data))))
  //     .pipe(map( data => {
  //       console.log("SDSD:", data);
  //       return data;

  //     }))
  //     .subscribe(
  //       resp => {
  //         console.log("Inside resp"+JSON.stringify(resp));
  //         this.promoList = <Promotion[]>resp;
  //         console.log(this.promoList);
  //         return this.promoList;
  //       },
  //       err => {

  //         console.log(err);
  //         return err;
  //       },
  //       () => console.log('Completed')
  //     );


  // }




  private handleError(err: Response){
    return Observable.throw(err.statusText);

  }
}
