import { arrayify } from 'tslint/lib/utils';
import { Promotion } from './dto/promo.dto';
import { selector } from 'rxjs/operator/publish';
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name : 'customAscendingArrayByNameSort'
})

export class CustomAscendingArrayByNameSort implements PipeTransform{

  sampleItem: object;

  transform(arrayList : any) {
    if (arrayList instanceof Array) {
        arrayList[0] = this.sampleItem;
        if (arrayList[0] instanceof Promotion) {
         return this.sortAscendingByName(<Promotion[]> arrayList);
        }

    } else {
      return arrayList;
    }
  }

  sortAscendingByName(promotionArray: Promotion[]) : Promotion[]{
    return promotionArray.sort( (a: Promotion, b: Promotion) => a.name.localeCompare(b.name) );
  }
}
