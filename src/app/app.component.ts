

import { environment } from '../environments/environment.prod';

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  name = 'vijay';
  ver:number = environment.VERSION;
  constructor(){
    console.log(this.ver);
  }
}
